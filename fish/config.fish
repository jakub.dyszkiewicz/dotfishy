#set -gx GOPATH /Users/jakub.dyszkiewicz/allegro/go
#set -gx PATH /usr/local/opt/php@5.6/bin $PATH
#set -gx PATH /usr/local/opt/php@5.6/sbin $PATH
#set -gx PATH $GOPATH/bin $PATH
#set -gx JAVA_HOME /Library/Java/JavaVirtualMachines/jdk1.8.0_172.jdk/Contents/Home
#set -gx GROOVY_HOME /usr/local/opt/groovy/libexec